class Game{
    constructor() {
        this.color = {
            empty: 0,
            black: 1,
            white: 2
        }
        this.gameEnd = false;
        this.turn = this.color.black;
    }
    changeTurn() {
        this.turn = this.getEnemyPlayer();
        if (this.turn != this.color.black) {
            if (this.turn != this.color.white) {
                throw new Error("error");
            }
        }
        return this.turn;
    }
    getTurn() {
        return this.turn;
    }
    getEnemyPlayer() {
        return 3 - this.turn;
    }

    playTurn(board, column, row, playerColor) {
        if (this.check(board, column, row, false, true) == true) {
            this.changeTurn();
        }
        if (this.pass(board) === true) {
            if(this.pass(board) === true){
                this.gemeover(board);
            }
        }
    }
    comTurn(board, comColor) {
        do {
            var column = Math.floor(Math.random() * 8) + 1;
            var row = Math.floor(Math.random() * 8) + 1;
            this.playTurn(board, column, row, comColor);
        } while (this.getTurn() === comColor);
    }
    pass(board) {
        if (this.checkAllPiece(board) === false) {
          this.changeTurn();
            return true;
        }
        return false;
    }
    gemeover(board) {
        this.gameEnd = true;
        var blackScore = 0;
        var whiteScore = 0;
        blackScore = this.countScore(board, board.color.black);
        whiteScore = this.countScore(board, board.color.white);
        showResult(blackScore, whiteScore);
    }

    countScore(board, color) {
        var score = 0;
        board.forBoard(function(column, row) {
            if (board.getPiece(column, row) === color)
                score++;
        });
        return score;
    }


    check(board, column, row, drawCheck, flip) {
        var ret = false; //石をひっくり返したか
        if (board.getPiece(column, row) !== 0) return false; //マスにすでに置かれているとき
        for (var dx = -1; dx <= 1; dx++) {
            ret = this.checkLengthwisePiece(board, column, row, dx, drawCheck, flip, ret);
        }
        return ret;
    }
    checkLengthwisePiece(board, column, row, dx, drawCheck, flip, ret) {
        for (var dy = -1; dy <= 1; dy++) {
            if (dx === 0 && dy === 0) continue;
            if (this.adjacentPiece(board, column, row, dx, dy, 0) > 1) { //隣接した石がいくつひっくり返せるか
                if (!drawCheck && flip) {
                    board.putPiece(column, row, this.getTurn());
                    this.adjacentPiece(board, column, row, dx, dy, 1); //実際にひっくり返す
                    ret = true;
                } else if (drawCheck) {
                    return true;
                }
            }
        }
        return ret;
    }
    adjacentPiece(board, column, row, dx, dy, rev) {
        if (board.getPiece(column + dx, row + dy) === this.getEnemyPlayer()) { //隣接した石が相手の石
            if (rev === 1) board.putPiece(column + dx, row + dy, this.getTurn());
            return 2 * this.adjacentPiece(board, column + dx, row + dy, dx, dy, rev);
        } else if (board.getPiece(column + dx, row + dy) === this.getTurn()) { //隣接した石が自分の石
            return 1;
        } else {
            return 0;
        }
    }
    checkAllPiece(board) {
        var this_ = this;
        var re = false;
        board.forBoard(function(column, row) {
            if (this_.check(board, column, row, true, false) === true) {
                re = true;
            }
        });
        return re;
    }
}
class Board {

    constructor() {
        this.color = {
            empty: 0,
            black: 1,
            white: 2
        }
        this.piece = new Array(10);
        this.boardInit();
    }
    boardInit() {
        var this_ = this;
        for (var pieceInit = 0; pieceInit < this.getPieceLength(); pieceInit++) {
            this.piece[pieceInit] = new Array(this.getPieceLength());
        }
        this.forBoard(function(column, row) {
            this_.piece[column][row] = this_.color.empty;
        });

        this.piece[4][5] = this.color.black;
        this.piece[5][4] = this.color.black;
        this.piece[4][4] = this.color.white;
        this.piece[5][5] = this.color.white;
    }

    putPiece(column, row, color) {

        this.piece[column][row] = color;
        if (color != this.color.black) {
            if (color != this.color.white) {
                throw new Error("error");
            }
        }
    }
    getPieceLength() {
        return this.piece.length;
    }
    getPiece(column, row) {
        return this.piece[column][row];
    }
    forBoard(func) {
        for (var column = 1; column < this.getPieceLength() - 1; column++) {
            this.forRow(func, column);
        }
    }
    forRow(func, column) {
        for (var row = 1; row < this.getPieceLength() - 1; row++) {
            func(column, row);
        }
    }

}

function setGame(game, board, playerColor) {
  setPlayerBlack(game, playerColor);
    draw(game, board);
}
function GameReset(game,board,playerColor){
  $("#gameset").hide();
  board.boardInit();
  draw(game, board);
}
function setPlayerBlack(game, playerColor) {
    playerColor[0] = game.color.black;
}

function setPlayerWhite(game, playerColor) {
    playerColor[0] = game.color.white;
}

function GameStart(game, board, playerColor) {
$('#othello').click(function(e) {
  if (game.getTurn() === playerColor[0]) {
    var column = ((e.offsetX) / 58 + 45 / 58) | 0;
    var row = ((e.offsetY) / 58 + 45 / 58) | 0;
      game.playTurn(board, column, row, game.getTurn());
  }
    if (game.getTurn() !== playerColor[0]){
        game.comTurn(board, game.getTurn());
      }
          draw(game, board);
        }
    );
}

function showResult(blackScore, whiteScore) {
    var message = (blackScore === whiteScore) ? "Draw" : (blackScore > whiteScore) ? "Black Win" : "White Win";
    $("#gameset").text(message + "     " + "Black:" + blackScore + "\t" + "White" + whiteScore);
  $("#gameset").show();
}


function draw(game, board) {
    if (game.gameEnd === false) {
        showTurn(game, board);
    }
    canvas = $("#othello").get(0);
    if (canvas.getContext) {
        var context = canvas.getContext('2d');
        context.drawImage(img_board, 0, 0, 500, 500);
        board.forBoard(function(column, row) {

            if (board.getPiece(column, row) == game.color.black) {
                context.drawImage(img_black, 13 + 58 * (column - 1), 10 + 58 * (row - 1), 80, 80);
            } else if (board.getPiece(column, row) === game.color.white) {
                context.drawImage(img_white, 13 + 58 * (column - 1), 10 + 58 * (row - 1), 80, 80);
            }

        });
    }
}

function showTurn(game, board) {

    if (game.getTurn() === game.color.black) {
        $("#turn").text("Turn:Black").val("a");
    } else if (game.getTurn() === game.color.white) {
        $("#turn").text("Turn:White").val("a");
    }
}
